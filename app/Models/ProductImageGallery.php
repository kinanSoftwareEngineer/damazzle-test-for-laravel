<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProductImageGallery extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id', 'path'
    ];

    public function getPathAttribute($value)
    {
        if(request()->method() == 'PUT' || request()->method() == 'DELETE')
            return str_replace('public', 'storage', $value);
        return request()->getHttpHost() . '/' . str_replace('public', 'storage', $value);
    }

}
