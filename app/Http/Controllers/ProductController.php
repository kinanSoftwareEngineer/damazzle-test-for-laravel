<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductImageGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $validator = Validator::make(request()->all(), [
            'latest' => 'sometimes|numeric|integer|gt:0'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $products = Product::query();
        $products = request()->query('latest') ? $products->latest('created_at')->limit(request()->query('latest')) : $products;
        if ($products->count() > 0)
            return response()->json(['payload' => $products->get()]);
        return response()->json('', 204);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:products,name',
            'price' => 'required|numeric|gt:0',
            'description' => 'required|string',
            'images' => 'required|array',
            'images.*' => 'required|file|image'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $data = $validator->valid();
        $images = $data['images'];
        unset($data['images']);

        try {
            DB::beginTransaction();
            $product = Product::create($data);
            foreach ($images as $image)
                ProductImageGallery::create([
                    'product_id' => $product->id,
                    'path' => $image->store('public/products')
                ]);
            DB::commit();
            $product->images = $product->images()->get();
            return response()->json(['payload' => $product], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return response()->json(['payload' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'sometimes|string|unique:products,name',
            'price' => 'sometimes|numeric|gt:0',
            'description' => 'sometimes|string',
            'images' => 'sometimes|array',
            'images.*' => 'required_with:images|file|image',
            'images_to_delete' => 'sometimes|array',
            'images_to_delete.*' => 'required_with:images_to_delete|numeric|integer|gt:0|exists:product_image_galleries,id'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $data = $validator->valid();

        try {
            DB::beginTransaction();
            if (array_key_exists('images', $data)) {
                foreach ($data['images'] as $image)
                    ProductImageGallery::create([
                        'product_id' => $product->id,
                        'path' => $image->store('public/products')
                    ]);
                unset($data['images']);
            }
            if(array_key_exists('images_to_delete', $data)) {
                foreach ($data['images_to_delete'] as $image) {
                    $image = ProductImageGallery::find($image);
                    unlink($image->path);
                    $image->delete();
                }
                unset($data['images_to_delete']);
            }
            $product->update($data);
            DB::commit();
            $product = Product::find($product->id);
            $product->images = $product->images()->get();
            foreach($product->images as $image)
                $image->path = request()->getHttpHost() . '/' . $image->path;
            return response()->json(['payload' => $product], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try{
            DB::beginTransaction();
            foreach($product->images as $image){
                unlink($image->path);
                $image->delete();
            }
            $product->delete();
            DB::commit();
            return response()->json(['payload' => 'Product deleted successfully.'], 200);
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
