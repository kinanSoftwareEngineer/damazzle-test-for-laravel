<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Sanctum\PersonalAccessToken;
use Laravel\Sanctum\Sanctum;

class Auth extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'profile_image' => 'required|file|image',
            'password' => 'required|string',
            'password_confirmation' => 'required_with:password'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $data = $validator->valid();
        $data['password'] = Hash::make($data['password']);
        $data['profile_image'] = $data['profile_image']->store('public/profile');

        if ($user = User::create($data))
            return response()->json(['payload' => $user], 200);
        return response()->json(['error' => $this->serverError], 500);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $data = $validator->valid();

        if ($user = User::where('email', $data['email'])->first())
            if (Hash::check($data['password'], $user->password)) {
                $user->token = $user->createToken($user->email)->plainTextToken;
                return response()->json(['payload' => $user], 200);
            }
        return response()->json(['error' => 'Incorrect email or password.'], 400);
    }

    public function logout(Request $request)
    {
        if ($request->user()->currentAccessToken()->delete()) {
            return response()->json(['payload' => 'User logged out successfully.']);
        }
        return response()->json(['error' => 'Unauthenticated.'], 401);
    }

    public function refreshToken(Request $request)
    {
        $user = $request->user();
        if ($user->tokens()->delete()) {
            $user->token = $user->createToken($user->email)->plainTextToken;
            return response()->json(['payload' => $user], 200);
        }
        return response()->json(['error' => $this->serverError], 500);
    }

    public function profile(Request $request)
    {
        return response()->json(['payload' => $request->user()], 200);
    }

    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'sometimes|email|unique:users,email',
            'first_name' => 'sometimes|string',
            'last_name' => 'sometimes|string',
            'profile_image' => 'sometimes|file|image',
            'password' => 'sometimes|string',
            'password_confirmation' => 'required_with:password'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $data = $validator->valid();
        $user = $request->user();

        if (array_key_exists('password', $data))
            $data['password'] = Hash::make($data['password']);
        if (array_key_exists('profile_image', $data)) {
            unlink($user->profile_image);
            $data['profile_image'] = $data['profile_image']->store('public/profile');
        }

        if ($user->update($data)) {
            $user = User::find($user->id);
            $user->profile_image = request()->getHttpHost() . '/' . $user->profile_image;
            return response()->json(['payload' => $user], 200);
        }
        return response()->json(['error' => $this->serverError], 500);
    }
}
