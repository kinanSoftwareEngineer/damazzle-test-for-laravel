<?php

use App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', [Auth::class, 'register']);
    Route::post('login', [Auth::class, 'login']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('me', [Auth::class, 'profile']);
        Route::put('me', [Auth::class, 'updateProfile']);
        Route::get('refresh', [Auth::class, 'refreshToken']);
        Route::get('logout', [Auth::class, 'logout']);
    });

    Route::apiResource('product', ProductController::class);
});
